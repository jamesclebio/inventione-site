<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Careers extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('careers');
    $this->setTitle('Inventione - Vagas');
    $this->setDescription('Soluções em tecnologia da informação');
    $this->setAnalytics(true);
    $this->setHeadAppend('<meta property="title" content="Faça parte da Inventione">');
    $this->mailchimp();
    $this->mail();
  }

  public function mailchimp() {
    if ($_POST && !$_POST['spamtrap'] && $_POST['mailing']) {
      require 'basis/helpers/mailchimp.php';

      // Inventione's account
      define('MAILCHIMP_API_KEY',  'a5ba0942ab3abdebf97967bcd625b7c1-us8');
      define('MAILCHIMP_LIST_ID',  '2ba4d5f400');

      // James's account
      // define('MAILCHIMP_API_KEY',  '6b7dbce2bb3b5b73d63c1abc00ae8979-us4');
      // define('MAILCHIMP_LIST_ID',  '546dc774c9');

      $mailchimp = new \MCAPI(MAILCHIMP_API_KEY);
      $fields = array(
        'MAIL_NAME' => $_POST['name'],
        'MAIL_PHONE' => $_POST['phone']
      );

      $mailchimp->listSubscribe(MAILCHIMP_LIST_ID, $_POST["email"], $fields, 'html', false, true);
    }
  }

  public function mail() {
    if ($_POST && !$_POST['spamtrap']) {
      if ($_FILES['curriculum']['type'] != 'application/pdf') {
        $this->setAlert('<p><strong>Ops! Arquivo inválido...</strong></p><p>Certifique-se de que o arquivo selecionado é um PDF de até 2MB.</p>', 'error');
      } else {
        $upload_file = $this->uploadPrepare();

        if (move_uploaded_file($_FILES['curriculum']['tmp_name'], $upload_file)) {
          $sender = 'inventione@inventione.com.br';
          $mail = new \Basis\Helpers\Mail();

          $mail->setHeader(
              'From: ' . $_POST['name'] . ' <' . $sender . '>' . "\r\n" .
              'Return-Path: ' . $sender . "\r\n" .
              'Reply-To: ' . $_POST['email'] . "\r\n"
          );

          $mail->setTo($sender);
          $mail->setSubject('[Inventione] Vagas');
          $mail->setBody(
              'Nome: ' . $_POST['name'] . "\n" .
              'E-mail: ' . $_POST['email'] . "\n" .
              'Telefone: ' . $_POST['phone'] . "\n\n" .
              '---' . "\n\n" .
              'Área de interesse: ' . $_POST['area'] . "\n" .
              'Currículo: http://' . $_SERVER['HTTP_HOST'] . $this->_url('root') . $upload_file . "\n" .
              'LinkedIn: ' . $_POST['linkedin'] . "\n" .
              'Informações adicionais: ' . $_POST['info'] . "\n\n" .
              '---' . "\n\n" .
              'Enviado a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
          );

          $mail->setAlertSuccess('<p><strong>Seus dados foram enviados com sucesso!</strong></p><p>Agradecemos pelo seu interesse em fazer parte da Inventione.</p>');
          $mail->send($this, $this->_url('vagas'), $this->_url('vagas'), '-r' . $sender);
        } else {
          $this->setAlert('<p><strong>Ops! Algo deu errado... :(</strong></p><p>Por favor, tente novamente.</p>', 'error');
        }
      }
    }
  }

  private function token($length = 20) {
    $token = null;
    $keys = range(0, 9);

    for ($i = 0; $i < $length; $i++) {
        $token .= $keys[array_rand($keys)];
    }

    return $token;
  }

  private function utf8($string, $from, $to) {
    $keys = array();
    $values = array();

    preg_match_all('/./u', $from, $keys);
    preg_match_all('/./u', $to, $values);

    $mapping = array_combine($keys[0], $values[0]);

    return strtr($string, $mapping);
  }

  private function plain($string) {
    $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
    $to = "aaaaeeiooouucAAAAEEIOOOUUC";

    return $this->utf8($string, $from, $to);
  }

  private function uploadPrepare() {
    $path = 'careers/';
    $filename = strtolower($this->token() . '-' . $this->plain(preg_replace('/(.+?)\s.*/', '$1', $_POST['name'])) . '.' . end(explode('.', $_FILES['curriculum']['name'])));

    return $path . $filename;
  }
}
