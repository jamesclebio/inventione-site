<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <contato@jamesclebio.com.br>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Index extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('index');
    $this->setTitle('Inventione');
    $this->setDescription('Soluções em tecnologia da informação');
    $this->setAnalytics(true);
  }
}
