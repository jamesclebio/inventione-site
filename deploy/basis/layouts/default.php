<!DOCTYPE html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?>" lang="pt-br">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta charset="utf-8">
    <title><?php echo $this->getTitle(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="<?php echo $this->getDescription(); ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="theme-color" content="#000">
    <meta property="og:site_name" content="Inventione">
    <meta property="og:image" content="http://inventione.com.br/assets/default/images/og-image.png">
    <!-- <link rel="apple-touch-icon" href="<?php echo $this->_asset('default/images/ico/60.png'); ?>"> -->
    <!-- <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->_asset('default/images/ico/76.png'); ?>"> -->
    <!-- <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->_asset('default/images/ico/120.png'); ?>"> -->
    <!-- <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->_asset('default/images/ico/152.png'); ?>"> -->
    <link rel="stylesheet" href="<?php echo $this->_asset('template/plugins/pace/pace-theme-flash.css'); ?>">
    <link rel="stylesheet" href="<?php echo $this->_asset('template/plugins/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo $this->_asset('template/plugins/font-awesome/css/font-awesome.css'); ?>">
    <link rel="stylesheet" href="<?php echo $this->_asset('template/plugins/swiper/css/swiper.css'); ?>" media="screen">
    <link rel="stylesheet" href="<?php echo $this->_asset('template/pages/css/pages.css'); ?>">
    <link rel="stylesheet" href="<?php echo $this->_asset('template/pages/css/pages-icons.css'); ?>">
    <link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
    <?php $this->getHeadAppend(); ?>
  </head>
  <body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?> pace-dark">
    <?php $this->getAnalytics(); ?>
    <?php $this->getBodyPrepend(); ?>

    <nav class="header bg-header transparent-dark " data-pages="header" data-pages-header="autoresize" data-pages-resize-class="dark">
      <div class="container relative">
        <div class="pull-left">
          <div class="header-inner">
            <a href="<?php echo $this->_url('root'); ?>"><img src="<?php echo $this->_asset('default/images/logo_black.png'); ?>" width="152" height="21" data-src-retina="<?php echo $this->_asset('default/images/logo_black_2x.png'); ?>" class="logo" alt=""></a>
            <a href="<?php echo $this->_url('root'); ?>"><img src="<?php echo $this->_asset('default/images/logo_white.png'); ?>" width="152" height="21" data-src-retina="<?php echo $this->_asset('default/images/logo_white_2x.png'); ?>" class="alt" alt=""></a>
          </div>
        </div>

        <div class="pull-right">
          <div class="header-inner">
            <div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10" data-pages="header-toggle" data-pages-element="#header">
              <div class="one"></div>
              <div class="two"></div>
              <div class="three"></div>
            </div>
          </div>
        </div>

        <div class="menu-content mobile-dark pull-right clearfix" data-pages-direction="slideRight" id="header">
          <div class="pull-right">
            <a href="#" class="padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10" data-pages="header-toggle" data-pages-element="#header">
              <i class=" pg-close_line"></i>
            </a>
          </div>

          <div class="header-inner">
            <ul class="menu">
              <li><a href="<?php echo $this->_url('root'); ?>" class="<?php echo ($this->getNameController() === 'index' ? ' active' : ''); ?>">Início</a></li>
              <li><a href="<?php echo $this->_url('vagas'); ?>" class="<?php echo ($this->getNameController() === 'careers' ? ' active' : ''); ?>">Vagas</a></li>
              <li><a href="<?php echo $this->_url('contato'); ?>" class="<?php echo ($this->getNameController() === 'contact' ? ' active' : ''); ?>">Contato</a></li>
            </ul>

            <div class="font-arial m-l-35 m-r-35 m-b-20 visible-sm visible-xs m-t-20">
              <p class="fs-11 small-text muted">&copy;2015 <strong>Inventione</strong></p>
            </div>
          </div>
        </div>
      </div>
    </nav>

    <div class="page-wrappers">
      <?php $this->getView(); ?>

      <footer class="p-b-50 p-t-50 bg-master-darker">
        <div class="container">
          <div class="text-center">
            <ul class="nav-footer no-style fs-11 no-padding font-arial">
              <li class="inline no-padding"><a href="<?php echo $this->_url('root'); ?>" class="text-white p-r-10 <?php echo ($this->getNameController() !== 'index' ? 'hint-text' : ''); ?>">Início</a></li>
              <li class="inline no-padding"><a href="<?php echo $this->_url('vagas'); ?>" class="text-white p-l-10 p-r-10 <?php echo ($this->getNameController() !== 'careers' ? 'hint-text' : ''); ?>">Vagas</a></li>
              <li class="inline no-padding"><a href="<?php echo $this->_url('contato'); ?>" class="text-white p-l-10 p-r-10 <?php echo ($this->getNameController() !== 'contact' ? 'hint-text' : ''); ?>">Contato</a></li>
              <li class="inline p-l-10 b-l b-grey"><a href="mailto:inventione@inventione.com.br" class="text-white"><i class="fa pg-mail m-r-10"></i>inventione@inventione.com.br</a></li>
            </ul>
            <div class="m-t-30">
              <img src="<?php echo $this->_asset('default/images/logo_white.png'); ?>" width="152" height="21" data-src-retina="<?php echo $this->_asset('default/images/logo_white_2x.png'); ?>" class="alt" alt="Logo Inventione">
              <p class="fs-11 text-white font-arial m-t-10 m-r-10 small-text hint-text">&copy;2015. Todos os direitos reservados.</p>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <script src="<?php echo $this->_asset('template/plugins/pace/pace.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo $this->_asset('template/pages/js/pages.image.loader.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/plugins/jquery/jquery-1.11.1.min.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/plugins/swiper/js/swiper.jquery.min.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/plugins/velocity/velocity.min.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/plugins/velocity/velocity.ui.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/plugins/jquery-unveil/jquery.unveil.min.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/pages/js/pages.frontend.js'); ?>"></script>
    <script src="<?php echo $this->_asset('template/js/custom.js'); ?>"></script>
    <script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
    <?php $this->getBodyAppend(); ?>
  </body>
</html>
