<section class="jumbotron full-vh" data-pages="parallax">
  <div class="inner full-height">
    <div class="swiper-container" id="hero">
      <div class="swiper-wrapper">

        <div class="swiper-slide fit">
          <div class="slider-wrapper">
            <div class="background-wrapper" data-swiper-parallax="30%">
              <div class="background" data-pages-bg-image="<?php echo $this->_asset('template/images/hero_1.jpg'); ?>"></div>
            </div>
          </div>
          <div class="content-layer">
            <div class="inner full-height">
              <div class="container-xs-height full-height">
                <div class="col-xs-height col-middle text-left">
                  <div class="container">
                    <div class="col-md-6 no-padding col-xs-10 col-xs-offset-1">
                      <h1 class="light sm-text-center" data-swiper-parallax="-15%">A <strong>Inventione</strong> tem <strong>inovação</strong> em seu DNA.</h1>
                      <h4 class="sm-text-center">Somos dedicados à arte de desenvolver mais que software, ideias disruptoras e novos conceitos.</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="mouse-wrapper">
        <div class="mouse">
          <div class="mouse-scroll"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="bg-master-lightest p-b-85 p-t-75">
  <div class="container">
    <div class="md-p-l-20 md-p-r-20 xs-no-padding">
      <h5 class="block-title hint-text no-margin">Vagas</h5>
      <div class="row">
        <div class="col-sm-5 col-md-4">
          <h1 class="m-t-5">Estamos contratando</h1>
          <p>Usam nossas soluções, pessoas e organizações com missões ambiciosas em diversas áreas de negócios, social ou governamental.</p>
          <a href="<?php echo $this->_url('vagas'); ?>" class="btn btn-lg btn-block btn-primary all-caps btn-cons m-t-20">Faça parte da <strong>Inventione</strong></a>
        </div>
        <div class="col-sm-7 col-md-8 no-padding xs-p-l-15 xs-p-r-15">
          <div class="p-t-20 p-l-35 md-p-l-5 md-p-t-15">
            <p>Nossos clientes contam conosco para entrega de soluções que envolvem tecnologia como diferencial competitivo e contribuem para o alcance de seus objetivos de negócio.</p>
            <p>Junte-se à Inventione e faça parte de um grupo de pessoas capazes, cujo propósito é criar soluções criativas de negócio, baseadas em software, com a capacidade de provocar uma mudança social positiva.</p>
            <p>Tecnologia, processos e problemas organizacionais são oportunidades de inovação e melhoria. Assim, buscamos grandes mentes em detrimento grandes egos, lideres e não algozes, modelos de hierarquia desconstruídos. Isso significa que você trabalhará lado a lado de desenvolvedores, analistas de qualidade e de negócio, pensando em soluções criativas para traduzir conceitos e ideias em práticas,  técnicas e produtos inovadoras.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
