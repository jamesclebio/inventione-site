<section class="container container-fixed-lg m-t-60 p-t-70 p-b-80  sm-p-t-30 sm-p-b-30">
  <?php $this->getAlert(); ?>

  <div class="m-b-30 text-center">
    <h1>Faça parte da <strong>Inventione</strong></h1>
    <div class="b-b b-grey m-t-30"></div>
  </div>

  <div class="row">
    <div class="col-md-5">
      <div class="row">
        <div class="col-sm-6 col-md-12">
          <h4><strong>E então, vai encarar o desafio?</strong><br>O candidato deve estar disposto a:</h4>
        </div>
        <div class="col-sm-6 col-md-12">
          <ul>
            <li>Inspirar os clientes para inovação</li>
            <li>Aprender com lições aprendidas e sucessos de um projeto, e compartilhar esse conhecimento com a organização como um todo.</li>
            <li>Nutrir o melhor ambiente de projeto e comprometimento, impulsionando o desempenho da equipe e provendo uma experiência ótima para o nossos colaboradores e clientes.</li>
            <li>Manter uma forte expertise e conhecimento das tecnologias e produtos de ponta</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-7">
      <br class=" visible-xs">
      <br class="visible-xs">
      <h4 class="sm-m-t-20"><strong>Seus dados</strong><br>Use o formulário para se candidatar:</h4>
      <form method="post" enctype="multipart/form-data" action="<?php echo $this->_url('vagas'); ?>" role="form">
        <input type="text" name="spamtrap" placeholder="Not fill this field">
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152">
        <div class="form-group form-group-default">
          <label>Nome *</label>
          <input name="name" type="text" class="form-control" required value="<?php echo (isset($_POST['name']) ? $_POST['name'] : '' ); ?>">
        </div>
        <div class="form-group form-group-default">
          <label>Email *</label>
          <input name="email" type="email" class="form-control" required value="<?php echo (isset($_POST['email']) ? $_POST['email'] : '' ); ?>">
        </div>
        <div class="form-group form-group-default">
          <label>Telefone *</label>
          <input name="phone" type="text" data-field-mask="phone" class="form-control" required value="<?php echo (isset($_POST['phone']) ? $_POST['phone'] : '' ); ?>">
        </div>
        <div class="b-b b-grey m-t-30 m-b-30"></div>
        <div class="form-group form-group-default">
          <label>Área(s) de interesse *</label>
          <input name="area" type="text" placeholder="Back-end, front-end, UX, etc." class="form-control" required value="<?php echo (isset($_POST['area']) ? $_POST['area'] : '' ); ?>">
        </div>
        <div class="form-group form-group-default">
          <label>Currículo *</label>
          <input name="curriculum" type="file" required value="<?php echo (isset($_POST['curriculum']) ? $_POST['curriculum'] : NULL); ?>">
          <p class="help-block small-text">Selecione um arquivo <strong>PDF</strong>, com até <strong>2MB</strong>.</p>
        </div>
        <div class="form-group form-group-default">
          <label>LinkedIn</label>
          <input name="linkedin" type="url" placeholder="Link do seu perfil no LinkedIn" class="form-control" value="<?php echo (isset($_POST['linkedin']) ? $_POST['linkedin'] : '' ); ?>">
        </div>
        <div class="form-group form-group-default">
          <label>Informações adicionais</label>
          <textarea name="info" class="form-control" style="height:100px"><?php echo (isset($_POST['info']) ? $_POST['info'] : '' ); ?></textarea>
        </div>
        <div class="checkbox check-primary">
          <input name="mailing" id="mailing" type="checkbox" value="1" checked>
          <label for="mailing">Desejo receber novidades da Inventione e seus parceiros</label>
        </div>
        <button class="btn btn-lg btn-primary font-montserrat all-caps fs-11 pull-right sm-m-t-10">Enviar</button>
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</section>

<section class="bg-master-lightest p-b-85 p-t-75">
  <div class="container">
    <div class="md-p-l-20 md-p-r-20 xs-no-padding">
      <h5 class="block-title hint-text no-margin">O que usamos</h5>
      <div class="row">
        <div class="col-sm-5 col-md-4">
          <h1 class="m-t-5">Tecnologias<br> e paradigmas de desenvolvimento<br> que utilizamos</h1>
        </div>
        <div class="col-sm-7 col-md-8 no-padding xs-p-l-15 xs-p-r-15">
          <img src="<?php echo $this->_asset('default/images/logo_set.png'); ?>" class="image-responsive-height lazy sm-m-t-30 loaded loaded" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
