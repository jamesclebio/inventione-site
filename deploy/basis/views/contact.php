<section class="container container-fixed-lg m-t-60 p-t-70 p-b-80  sm-p-t-30 sm-p-b-30">
  <?php $this->getAlert(); ?>

  <div class="m-b-30 text-center">
    <h1>Fale com a <strong>Inventione</strong></h1>
    <p>Você pode usar o formulário abaixo ou escrever diretamente para <a href="inventione@inventione.com.br">inventione@inventione.com.br</a></p>
    <div class="b-b b-grey m-t-30"></div>
  </div>

  <form method="post" action="<?php echo $this->_url('contato'); ?>" role="form">
    <input type="text" name="spamtrap" placeholder="Not fill this field">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group form-group-default">
          <label>Nome *</label>
          <input name="name" type="text" class="form-control" required value="<?php echo (isset($_POST['name']) ? $_POST['name'] : '' ); ?>">
        </div>
        <div class="form-group form-group-default">
          <label>Email *</label>
          <input name="email" type="email" class="form-control" required value="<?php echo (isset($_POST['email']) ? $_POST['email'] : '' ); ?>">
        </div>
        <div class="form-group form-group-default">
          <label>Telefone *</label>
          <input name="phone" type="text" data-field-mask="phone" class="form-control" required value="<?php echo (isset($_POST['phone']) ? $_POST['phone'] : '' ); ?>">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group form-group-default">
          <label>Mensagem *</label>
          <textarea name="message" class="form-control" style="height:154px" required><?php echo (isset($_POST['message']) ? $_POST['message'] : '' ); ?></textarea>
        </div>
        <div class="checkbox check-primary">
          <input name="mailing" id="mailing" type="checkbox" value="1" checked>
          <label for="mailing">Desejo receber novidades da Inventione e seus parceiros</label>
        </div>
        <div class="text-right">
          <button class="btn btn-lg btn-primary font-montserrat all-caps fs-11 sm-m-t-10">Enviar</button>
        </div>
      </div>
    </div>
  </form>
</section>

<section class="bg-master-lightest p-b-50 p-t-50 ">
  <div class="container p-b-50 p-t-50">
    <div class="text-center">
      <h1>A <strong>Inventione</strong> está em busca de novos talentos</h1>
      <a href="<?php echo $this->_url('vagas'); ?>" class="btn btn-lg btn-primary btn-cons all-caps m-t-20">Faça parte da <strong>Inventione</strong></a>
      <p class="m-t-30">Junte-se a Inventione e faça parte de um grupo de pessoas capazes,<br> cujo propósito é criar soluções criativas de negócio, baseadas em software,<br> com a capacidade de provocar uma mudança social positiva.</p>
    </div>
  </div>
</section>
