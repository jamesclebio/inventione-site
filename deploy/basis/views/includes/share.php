<ul class="list-icon">
	<li><a href="#" data-share="facebook" title="Compartilhar no Facebook"><span class="icon-social-facebook"></span></a></li>
	<li><a href="#" data-share="twitter" title="Compartilhar no Twitter"><span class="icon-social-twitter"></span></a></li>
	<li><a href="#" data-share="googleplus" title="Compartilhar no Google+"><span class="icon-social-googleplus"></span></a></li>
	<li><a href="#" data-share="pinterest" title="Compartilhar no Pinterest"><span class="icon-social-pinterest"></span></a></li>
	<li><a href="#" data-share="linkedin" title="Compartilhar no LinkedIn"><span class="icon-social-linkedin"></span></a></li>
</ul>
