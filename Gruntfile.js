module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    project: {
      banner: '/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',

      default: {
        scripts: {
          base: [
            'bower_components/jquery-maskedinput/dist/jquery.maskedinput.js',
            'bower_components/jquery-maskmoney/dist/jquery.maskMoney.js'
          ],

          main: [
            '<%= project.default.scripts.base %>',
            'assets/default/scripts/main.*.js',
            'assets/default/scripts/main.js'
          ]
        },

        styles: {
          path: 'assets/default/styles',
          main: '<%= project.default.styles.path %>/**/*.sass'
        }
      }
    },

    jshint: {
      options: {
        force: true
      },

      default: 'assets/default/scripts/**/*'
    },

    uglify: {
      options: {
        banner: '<%= project.banner %>\n'
      },

      default: {
        files: {
          'deploy/assets/default/scripts/main.js': '<%= project.default.scripts.main %>'
        }
      }
    },

    imagemin: {
      default: {
        files: [{
          expand: true,
          cwd: 'assets/default/images/',
          src: '**/*.{png,jpg,gif}',
          dest: 'deploy/assets/default/images/'
        }]
      }
    },

    compass: {
      options: {
        banner: '<%= project.banner %>',
        relativeAssets: true,
        noLineComments: true,
        outputStyle: 'compressed',
        raw: 'preferred_syntax = :sass\nrequire "sass-css-importer"\n'
      },

      default: {
        options: {
          sassDir: '<%= project.default.styles.path %>',
          specify: '<%= project.default.styles.main %>',
          cssDir: 'deploy/assets/default/styles',
          fontsDir: 'deploy/assets/default/fonts',
          imagesDir: 'deploy/assets/default/images'
        }
      }
    },

    copy: {
      fonts: {
        expand: true,
        flatten: true,
        filter: 'isFile',
        src: 'assets/default/fonts/**',
        dest: 'deploy/assets/default/fonts/'
      }
    },

    clean: {
      default: 'deploy/assets/default',
      scripts: 'deploy/assets/default/scripts',
      styles: 'deploy/assets/default/styles'
    },

    watch: {
      options: {
        livereload: true,
        spawn: false
      },

      gruntfile: {
        files: 'Gruntfile.js',
        tasks: 'default'
      },

      scripts: {
        files: ['<%= jshint.default %>'],
        tasks: ['scripts']
      },

      styles: {
        files: ['<%= project.default.styles.main %>'],
        tasks: ['styles']
      },

      app: {
        files: ['deploy/**/*.html']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-cache-bust');

  grunt.registerTask('default', ['clean:default', 'fonts', 'images', 'scripts', 'styles']);
  grunt.registerTask('fonts', ['copy:fonts']);
  grunt.registerTask('images', ['imagemin']);
  grunt.registerTask('scripts', ['jshint', 'clean:scripts', 'uglify']);
  grunt.registerTask('styles', ['clean:styles', 'compass']);
};
